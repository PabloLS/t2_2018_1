CC=gcc

CFLAGS=-Wall -Wextra -Werror -std=c11 -O0 -g -lm
LDLIBS=-lm
all: grade

sim.o: sim.c


testme: sim.o test.c
	$(CC) $(CFLAGS) -o testme sim.o test.c

grade: testme
	./testme

aluno: sim.o aluno.c 
	$(CC) $(CFLAGS) -o aluno sim.o aluno.c

run_aluno: aluno
	./aluno

clean:
	rm -rf *.o sim
